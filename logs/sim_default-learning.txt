/-----------------------------------------
| State-action rewards from Q-Learning
\-----------------------------------------

('left', 'green', 'forward', 'left')
 -- forward : 0.00
 -- right : 0.00
 -- None : -2.71
 -- left : -9.84

('right', 'green', 'left', 'right')
 -- forward : 0.24
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('left', 'red', 'forward', 'forward')
 -- forward : 0.00
 -- right : 0.00
 -- None : 1.99
 -- left : 0.00

('right', 'red', 'forward', None)
 -- forward : -7.74
 -- right : 0.00
 -- None : 0.25
 -- left : -7.22

('right', 'green', 'forward', 'right')
 -- forward : 0.00
 -- right : 0.75
 -- None : 0.00
 -- left : 0.00

('left', 'green', 'left', None)
 -- forward : 0.00
 -- right : 0.00
 -- None : -2.65
 -- left : 1.35

('left', 'red', 'right', 'forward')
 -- forward : -20.00
 -- right : 0.00
 -- None : 0.00
 -- left : -20.00

('left', 'red', 'right', 'left')
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.00
 -- left : -20.05

('left', 'red', 'forward', None)
 -- forward : -8.43
 -- right : 0.00
 -- None : 1.52
 -- left : 0.00

('right', 'red', 'right', None)
 -- forward : 0.00
 -- right : 1.47
 -- None : 0.00
 -- left : 0.00

('forward', 'red', None, 'forward')
 -- forward : -37.44
 -- right : -17.21
 -- None : 1.60
 -- left : 0.00

('left', 'green', 'right', 'left')
 -- forward : 0.00
 -- right : 0.00
 -- None : -2.40
 -- left : 0.00

('forward', 'green', 'left', 'left')
 -- forward : 0.00
 -- right : 0.57
 -- None : 0.00
 -- left : 0.00

('right', 'green', None, 'left')
 -- forward : 0.19
 -- right : 0.73
 -- None : 0.00
 -- left : 0.68

('forward', 'green', 'right', 'forward')
 -- forward : 0.12
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('forward', 'red', None, None)
 -- forward : -7.12
 -- right : -0.13
 -- None : 2.00
 -- left : -9.38

('left', 'green', None, None)
 -- forward : 0.36
 -- right : 0.00
 -- None : -4.65
 -- left : 1.87

('left', 'red', None, 'forward')
 -- forward : 0.00
 -- right : -10.14
 -- None : 1.51
 -- left : 0.00

('right', 'green', 'left', 'left')
 -- forward : 0.00
 -- right : 0.00
 -- None : -2.00
 -- left : 0.00

('forward', 'red', None, 'right')
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.84
 -- left : 0.00

('left', 'red', 'left', 'left')
 -- forward : 0.00
 -- right : 0.92
 -- None : 0.00
 -- left : 0.00

('left', 'green', 'right', None)
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('right', 'red', None, None)
 -- forward : -9.11
 -- right : 1.30
 -- None : 1.26
 -- left : -4.62

('forward', 'green', 'forward', None)
 -- forward : 0.00
 -- right : 0.76
 -- None : 0.00
 -- left : 0.00

('right', 'red', 'left', 'right')
 -- forward : 0.00
 -- right : 0.60
 -- None : 0.00
 -- left : 0.00

('right', 'green', None, 'right')
 -- forward : 0.00
 -- right : 1.11
 -- None : -2.59
 -- left : 0.00

('left', 'red', None, 'left')
 -- forward : -8.39
 -- right : 0.78
 -- None : 0.00
 -- left : -20.10

('forward', 'red', None, 'left')
 -- forward : 0.00
 -- right : 0.29
 -- None : 1.26
 -- left : 0.00

('forward', 'red', 'forward', 'forward')
 -- forward : -20.15
 -- right : 0.00
 -- None : 1.08
 -- left : 0.00

('left', 'red', 'forward', 'right')
 -- forward : -4.81
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('right', 'red', None, 'forward')
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.26
 -- left : -30.29

('right', 'green', 'right', 'left')
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('forward', 'red', 'left', None)
 -- forward : -4.59
 -- right : 0.39
 -- None : 0.00
 -- left : -5.18

('forward', 'green', 'left', 'forward')
 -- forward : 1.23
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('left', 'red', 'left', 'forward')
 -- forward : 0.00
 -- right : -14.81
 -- None : 1.43
 -- left : -19.79

('right', 'red', 'forward', 'forward')
 -- forward : -19.93
 -- right : 0.00
 -- None : 0.00
 -- left : -29.84

('forward', 'green', 'forward', 'left')
 -- forward : 0.00
 -- right : 0.00
 -- None : -2.60
 -- left : 0.00

('forward', 'red', 'forward', 'left')
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('right', 'red', 'forward', 'right')
 -- forward : -5.43
 -- right : 0.00
 -- None : 0.78
 -- left : -4.72

('forward', 'green', 'forward', 'right')
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('right', 'red', 'left', None)
 -- forward : 0.00
 -- right : 1.79
 -- None : 0.74
 -- left : -4.95

('right', 'green', 'forward', None)
 -- forward : 0.34
 -- right : 0.92
 -- None : -4.18
 -- left : -9.68

('forward', 'green', 'left', None)
 -- forward : 1.26
 -- right : 0.52
 -- None : 0.00
 -- left : -0.11

('right', 'red', None, 'right')
 -- forward : -4.57
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('forward', 'green', 'right', 'left')
 -- forward : 0.00
 -- right : 0.81
 -- None : 0.00
 -- left : 0.00

('left', 'green', 'left', 'forward')
 -- forward : 0.00
 -- right : 0.84
 -- None : 0.00
 -- left : 0.00

('left', 'green', None, 'left')
 -- forward : -0.32
 -- right : 0.62
 -- None : -4.92
 -- left : 1.96

('left', 'red', 'forward', 'left')
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.35
 -- left : -5.25

('right', 'green', 'left', None)
 -- forward : 0.00
 -- right : 1.53
 -- None : -2.83
 -- left : 0.75

('forward', 'green', None, 'forward')
 -- forward : 1.40
 -- right : 0.00
 -- None : -2.28
 -- left : 0.00

('right', 'red', 'forward', 'left')
 -- forward : -4.51
 -- right : 0.00
 -- None : 0.43
 -- left : -8.75

('left', 'green', 'left', 'left')
 -- forward : 0.00
 -- right : 0.38
 -- None : 0.00
 -- left : 0.00

('right', 'green', 'left', 'forward')
 -- forward : 0.00
 -- right : 0.81
 -- None : 0.00
 -- left : 0.00

('left', 'green', 'left', 'right')
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.00
 -- left : 0.74

('left', 'red', 'right', None)
 -- forward : 0.00
 -- right : 0.51
 -- None : 0.00
 -- left : 0.00

('forward', 'green', 'right', None)
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.00
 -- left : -9.82

('left', 'red', None, None)
 -- forward : -9.45
 -- right : 0.00
 -- None : 1.50
 -- left : -5.06

('right', 'green', 'right', None)
 -- forward : 0.00
 -- right : 1.28
 -- None : 0.00
 -- left : 0.00

('forward', 'red', 'right', 'forward')
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('forward', 'green', None, None)
 -- forward : 2.25
 -- right : 0.00
 -- None : -4.04
 -- left : 0.49

('right', 'red', 'right', 'right')
 -- forward : -4.75
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('forward', 'green', None, 'right')
 -- forward : 0.00
 -- right : 0.42
 -- None : 0.00
 -- left : 0.00

('left', 'green', None, 'forward')
 -- forward : 0.00
 -- right : 0.71
 -- None : -2.81
 -- left : 1.17

('forward', 'red', 'left', 'left')
 -- forward : -5.09
 -- right : 0.00
 -- None : 0.00
 -- left : -4.53

('forward', 'green', None, 'left')
 -- forward : 1.10
 -- right : 0.00
 -- None : 0.00
 -- left : 0.37

('right', 'green', None, None)
 -- forward : 0.78
 -- right : 1.44
 -- None : -2.80
 -- left : 0.67

('forward', 'red', 'forward', None)
 -- forward : 0.00
 -- right : 1.21
 -- None : 0.00
 -- left : -19.52

('forward', 'red', 'right', None)
 -- forward : 0.00
 -- right : 1.13
 -- None : 1.21
 -- left : 0.00

('left', 'green', 'forward', None)
 -- forward : -0.05
 -- right : 0.67
 -- None : -2.34
 -- left : -15.17

('right', 'red', 'left', 'left')
 -- forward : -4.70
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('right', 'green', 'forward', 'left')
 -- forward : 0.00
 -- right : 0.00
 -- None : -2.98
 -- left : 0.00

('forward', 'green', 'forward', 'forward')
 -- forward : 0.84
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('left', 'green', None, 'right')
 -- forward : 0.80
 -- right : 0.00
 -- None : -2.69
 -- left : 0.00

('left', 'red', 'left', None)
 -- forward : -4.67
 -- right : 0.18
 -- None : 1.31
 -- left : -5.29

('right', 'green', None, 'forward')
 -- forward : 0.00
 -- right : 1.26
 -- None : -4.26
 -- left : 0.00

('left', 'red', 'left', 'right')
 -- forward : 0.00
 -- right : 0.30
 -- None : 0.00
 -- left : 0.00

('right', 'red', None, 'left')
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.15
 -- left : 0.00

('right', 'green', 'right', 'forward')
 -- forward : 0.00
 -- right : 0.00
 -- None : -2.14
 -- left : 0.00

