/-----------------------------------------
| State-action rewards from Q-Learning
\-----------------------------------------

('left', 'green', 'forward', 'left')
 -- forward : 0.87
 -- right : 0.02
 -- None : -4.71
 -- left : -19.62

('right', 'green', 'forward', 'right')
 -- forward : 0.71
 -- right : 1.88
 -- None : -4.48
 -- left : -18.91

('left', 'red', 'right', 'left')
 -- forward : -23.74
 -- right : 0.84
 -- None : 2.54
 -- left : -38.93

('right', 'red', 'right', None)
 -- forward : -9.65
 -- right : 1.57
 -- None : 0.09
 -- left : -40.68

('right', 'green', 'right', 'forward')
 -- forward : 0.84
 -- right : 1.87
 -- None : -4.59
 -- left : -17.56

('left', 'red', 'right', 'right')
 -- forward : -5.18
 -- right : 0.75
 -- None : 1.95
 -- left : -19.78

('right', 'green', None, 'left')
 -- forward : 1.07
 -- right : 1.33
 -- None : -4.74
 -- left : 0.65

('left', 'green', 'left', 'left')
 -- forward : 0.05
 -- right : 1.48
 -- None : -4.82
 -- left : 2.32

('forward', 'red', None, None)
 -- forward : -25.76
 -- right : 0.69
 -- None : 1.62
 -- left : -17.65

('right', 'green', 'left', 'right')
 -- forward : 0.82
 -- right : 2.12
 -- None : -5.09
 -- left : 0.48

('right', 'red', None, 'forward')
 -- forward : -40.57
 -- right : -19.60
 -- None : 1.28
 -- left : -40.01

('forward', 'red', 'left', None)
 -- forward : -10.43
 -- right : 0.85
 -- None : 1.66
 -- left : -10.84

('left', 'red', 'left', 'forward')
 -- forward : -39.82
 -- right : -19.60
 -- None : 1.98
 -- left : -39.89

('right', 'red', 'right', 'left')
 -- forward : -25.08
 -- right : 1.93
 -- None : 0.31
 -- left : -39.82

('left', 'red', None, None)
 -- forward : -13.44
 -- right : 1.02
 -- None : 1.50
 -- left : -11.02

('forward', 'green', 'left', None)
 -- forward : 1.16
 -- right : 0.95
 -- None : -5.35
 -- left : 1.00

('left', 'green', 'left', 'forward')
 -- forward : 0.79
 -- right : 0.87
 -- None : -4.76
 -- left : 0.84

('right', 'green', 'right', None)
 -- forward : 0.74
 -- right : 2.12
 -- None : -5.56
 -- left : -19.90

('right', 'red', 'left', 'forward')
 -- forward : -39.37
 -- right : -20.29
 -- None : 0.65
 -- left : -40.00

('left', 'green', None, 'right')
 -- forward : 1.11
 -- right : 0.46
 -- None : -5.10
 -- left : 1.93

('forward', 'red', 'right', 'left')
 -- forward : -29.49
 -- right : 0.95
 -- None : 1.70
 -- left : -30.16

('right', 'red', 'right', 'forward')
 -- forward : -30.59
 -- right : -17.94
 -- None : 0.70
 -- left : -29.90

('left', 'green', 'right', 'right')
 -- forward : 0.76
 -- right : 0.22
 -- None : -2.24
 -- left : -19.01

('forward', 'green', None, None)
 -- forward : 1.94
 -- right : 0.41
 -- None : -4.89
 -- left : 0.57

('right', 'green', 'forward', 'left')
 -- forward : 0.63
 -- right : 1.64
 -- None : -4.98
 -- left : -14.97

('left', 'red', 'left', None)
 -- forward : -9.42
 -- right : 0.22
 -- None : 1.22
 -- left : -9.87

('right', 'green', None, 'forward')
 -- forward : 1.09
 -- right : 1.37
 -- None : -5.12
 -- left : 1.14

('left', 'green', 'left', None)
 -- forward : 0.52
 -- right : 0.70
 -- None : -5.17
 -- left : 1.95

('left', 'red', 'forward', None)
 -- forward : -9.93
 -- right : 0.85
 -- None : 1.68
 -- left : -9.52

('left', 'green', 'right', 'left')
 -- forward : 1.02
 -- right : 0.29
 -- None : -4.66
 -- left : -18.44

('left', 'red', None, 'right')
 -- forward : -10.66
 -- right : 0.59
 -- None : 2.26
 -- left : -9.54

('forward', 'green', 'right', 'forward')
 -- forward : 2.11
 -- right : 0.31
 -- None : -2.17
 -- left : -14.66

('right', 'green', 'left', 'left')
 -- forward : 1.04
 -- right : 2.33
 -- None : -5.04
 -- left : 0.49

('left', 'red', 'left', 'left')
 -- forward : -9.96
 -- right : 0.68
 -- None : 1.25
 -- left : -10.81

('right', 'red', 'left', 'right')
 -- forward : -11.05
 -- right : 2.13
 -- None : 0.60
 -- left : -25.05

('forward', 'red', 'forward', 'forward')
 -- forward : -39.71
 -- right : -17.55
 -- None : 2.00
 -- left : -38.45

('left', 'red', 'forward', 'right')
 -- forward : -10.17
 -- right : 0.43
 -- None : 1.11
 -- left : -24.70

('left', 'green', 'forward', 'right')
 -- forward : 0.24
 -- right : 0.28
 -- None : -4.84
 -- left : -19.47

('forward', 'red', 'forward', 'left')
 -- forward : -9.60
 -- right : 0.68
 -- None : 1.86
 -- left : -18.51

('forward', 'green', 'forward', 'right')
 -- forward : 2.10
 -- right : 0.76
 -- None : -4.89
 -- left : -18.90

('forward', 'red', 'right', 'right')
 -- forward : -10.06
 -- right : 0.55
 -- None : 1.50
 -- left : -35.15

('right', 'red', None, 'right')
 -- forward : -27.31
 -- right : 1.78
 -- None : 0.13
 -- left : -11.20

('left', 'green', 'forward', None)
 -- forward : 0.08
 -- right : 0.70
 -- None : -5.15
 -- left : -20.20

('forward', 'red', 'right', 'forward')
 -- forward : -39.02
 -- right : -17.13
 -- None : 1.63
 -- left : -34.26

('forward', 'green', 'right', None)
 -- forward : 1.87
 -- right : 0.81
 -- None : -4.88
 -- left : -19.82

('forward', 'red', None, 'right')
 -- forward : -13.96
 -- right : 0.65
 -- None : 1.74
 -- left : -10.27

('right', 'green', None, None)
 -- forward : 0.54
 -- right : 1.64
 -- None : -5.29
 -- left : 1.15

('forward', 'red', 'forward', None)
 -- forward : -10.00
 -- right : 0.93
 -- None : 1.50
 -- left : -10.76

('forward', 'red', None, 'left')
 -- forward : -32.87
 -- right : 1.29
 -- None : 1.23
 -- left : -10.19

('forward', 'green', 'forward', 'forward')
 -- forward : 2.15
 -- right : 1.05
 -- None : -5.32
 -- left : -19.73

('right', 'red', None, 'left')
 -- forward : -25.65
 -- right : 1.71
 -- None : 0.11
 -- left : -10.93

('forward', 'green', 'left', 'right')
 -- forward : 2.19
 -- right : 0.90
 -- None : -4.61
 -- left : 0.50

('right', 'green', 'forward', 'forward')
 -- forward : 0.57
 -- right : 1.99
 -- None : -3.94
 -- left : -18.78

('forward', 'red', 'forward', 'right')
 -- forward : -10.28
 -- right : 0.30
 -- None : 1.97
 -- left : -9.71

('right', 'green', None, 'right')
 -- forward : 0.49
 -- right : 2.49
 -- None : -5.39
 -- left : 0.62

('forward', 'red', 'left', 'right')
 -- forward : -10.19
 -- right : 0.79
 -- None : 2.19
 -- left : -10.03

('forward', 'red', None, 'forward')
 -- forward : -39.98
 -- right : -20.11
 -- None : 1.73
 -- left : -39.21

('forward', 'green', 'left', 'left')
 -- forward : 1.85
 -- right : 0.73
 -- None : -5.20
 -- left : 1.38

('right', 'green', 'right', 'left')
 -- forward : 0.00
 -- right : 2.38
 -- None : -4.34
 -- left : -15.13

('left', 'red', None, 'forward')
 -- forward : -39.85
 -- right : -19.74
 -- None : 1.60
 -- left : -39.55

('forward', 'red', 'right', None)
 -- forward : -25.23
 -- right : 0.55
 -- None : 1.29
 -- left : -39.48

('left', 'green', 'right', 'forward')
 -- forward : 0.43
 -- right : 0.99
 -- None : -3.98
 -- left : -17.91

('right', 'red', None, None)
 -- forward : -16.77
 -- right : 1.63
 -- None : 1.13
 -- left : -18.51

('forward', 'green', 'forward', None)
 -- forward : 1.91
 -- right : 1.01
 -- None : -5.36
 -- left : -19.94

('forward', 'red', 'left', 'forward')
 -- forward : -39.62
 -- right : -19.96
 -- None : 2.40
 -- left : -39.66

('left', 'red', None, 'left')
 -- forward : -19.78
 -- right : 0.81
 -- None : 1.69
 -- left : -9.74

('forward', 'green', 'left', 'forward')
 -- forward : 2.17
 -- right : 0.28
 -- None : -5.24
 -- left : 0.43

('right', 'red', 'forward', 'forward')
 -- forward : -39.64
 -- right : -18.86
 -- None : 0.54
 -- left : -39.37

('right', 'red', 'left', None)
 -- forward : -10.15
 -- right : 1.66
 -- None : 1.04
 -- left : -11.91

('right', 'green', 'forward', None)
 -- forward : 0.43
 -- right : 2.04
 -- None : -5.00
 -- left : -20.19

('forward', 'green', None, 'forward')
 -- forward : 1.74
 -- right : 1.09
 -- None : -4.99
 -- left : 1.23

('right', 'red', 'forward', 'left')
 -- forward : -11.22
 -- right : 1.63
 -- None : 0.43
 -- left : -8.33

('forward', 'green', None, 'right')
 -- forward : 2.09
 -- right : 0.85
 -- None : -5.39
 -- left : 1.10

('forward', 'green', 'forward', 'left')
 -- forward : 2.04
 -- right : 1.08
 -- None : -4.82
 -- left : -20.09

('right', 'green', 'left', 'forward')
 -- forward : 0.48
 -- right : 2.29
 -- None : -5.13
 -- left : 0.59

('left', 'red', 'forward', 'left')
 -- forward : -13.44
 -- right : 0.83
 -- None : 2.13
 -- left : -10.50

('left', 'green', None, 'forward')
 -- forward : 0.13
 -- right : 1.08
 -- None : -5.20
 -- left : 1.58

('forward', 'red', 'left', 'left')
 -- forward : -25.64
 -- right : 1.15
 -- None : 2.21
 -- left : -10.06

('left', 'red', 'right', 'forward')
 -- forward : -37.12
 -- right : -18.19
 -- None : 1.89
 -- left : -37.40

('left', 'green', 'left', 'right')
 -- forward : 1.40
 -- right : 0.26
 -- None : -5.11
 -- left : 2.32

('right', 'red', 'left', 'left')
 -- forward : -11.49
 -- right : 1.40
 -- None : 1.37
 -- left : -17.65

('left', 'red', 'left', 'right')
 -- forward : -10.42
 -- right : 1.07
 -- None : 1.50
 -- left : -13.54

('forward', 'green', 'right', 'right')
 -- forward : 1.88
 -- right : 0.40
 -- None : -2.43
 -- left : -10.43

('left', 'red', 'forward', 'forward')
 -- forward : -38.32
 -- right : -20.23
 -- None : 1.78
 -- left : -39.59

('right', 'red', 'forward', None)
 -- forward : -10.27
 -- right : 2.10
 -- None : 0.72
 -- left : -18.22

('forward', 'green', None, 'left')
 -- forward : 1.57
 -- right : 0.97
 -- None : -4.98
 -- left : 0.76

('left', 'green', 'right', None)
 -- forward : 0.15
 -- right : 0.98
 -- None : -4.41
 -- left : -19.91

('left', 'green', None, None)
 -- forward : 0.58
 -- right : 0.75
 -- None : -5.71
 -- left : 2.02

('right', 'green', 'right', 'right')
 -- forward : 0.47
 -- right : 0.52
 -- None : -4.27
 -- left : 0.00

('forward', 'green', 'right', 'left')
 -- forward : 2.35
 -- right : 0.82
 -- None : -4.51
 -- left : -19.36

('right', 'red', 'forward', 'right')
 -- forward : -17.17
 -- right : 2.27
 -- None : 1.17
 -- left : -9.26

('left', 'green', 'forward', 'forward')
 -- forward : 0.28
 -- right : 0.86
 -- None : -4.68
 -- left : -20.11

('left', 'green', None, 'left')
 -- forward : 0.83
 -- right : 1.23
 -- None : -4.85
 -- left : 1.44

('right', 'green', 'left', None)
 -- forward : 0.31
 -- right : 1.25
 -- None : -4.51
 -- left : 0.45

('left', 'red', 'right', None)
 -- forward : -10.32
 -- right : 0.54
 -- None : 1.99
 -- left : -39.83

('right', 'red', 'right', 'right')
 -- forward : 0.00
 -- right : 2.49
 -- None : 0.31
 -- left : -20.49

